<?php
    include_once 'includes/dbc.php';
    include_once 'includes/products.php';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- main css -->
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>
    <!-- NAVIGATION -->
    <nav class="navbar navbar-expand navbar-dark bg-dark">
        <a class="navbar-brand" href="/">Products</a>
        <a class="navbar-brand" href="/add.php">Add products</a>
    </nav>
    <!-- end of NAVIGATION -->
    <!-- Product "add" section-->
    <div class="container">
        <form class="needs-validation" method="POST" validate>
            <div class="row">
                <div class="col-6 mt-3">
                    <label for="sku">SKU</label>
                    <small>*Must be unique value</small>
                    <input type="text" class="form-control" id="sku" name="sku" required>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <label for="product">Product name</label>
                    <input type="text" class="form-control" id="productName" name="productName" required>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <label for="price">Product price</label>
                    <input type="text" class="form-control" id="productPrice" name="productPrice" required>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <label for="type">Product type</label>
                    <select class="form-control" id="productType" name="productType" required>
                        <option value="-1">Select product type</option>
                        <option value="1" name="book">Book</option>
                        <option value="2" name="cd">CD</option>
                        <option value="3" name="furniture">Furniture</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <label for="atribute">Atribute</label><br>
                    <small class="items alert-danger item-1">Please select product type</small>
                    <small class="items alert-danger item1">Please provide weight in kg</small>
                    <small class="items alert-danger item2">Please provide amount of memory in Mb</small>
                    <small class="items alert-danger item3">Please provide dimensions in HxWxL format</small>
                    <span class="form-inline">
                        <input type="text" class="form-control col-11 form-inline" id="productAtribute" name="productAtribute" required>&nbsp;
                        <span class ="items item1">kg</span>
                        <span class ="items item2">Mb</span>
                        <span class ="items item3">cm</span>
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mt-3">
                    <button class="col-3 btn btn-success btn-lg btn-block" type="submit" name="add">Add product</button>
                </div>
            </div>
        </form>
    </div>
    <?php

        
        if(isset($_POST['add'])) {

            $sku = $_POST['sku'];
            $name = $_POST['productName'];
            $price = $_POST['productPrice'];
            $type = $_POST['productType'];
            $att = $_POST['productAtribute'];

            $pro = new Products();
            $pro->addProduct($sku, $name, $price, $type, $att);

        }

    ?>
    <!-- end of product "add" section-->
    <!-- jQuery js -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <!-- custom js -->
    <script src="/assets/main.js"></script>
    <!-- bootsrap jQuery js -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <!-- bootstrap js -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- bootsrap popper js -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
</body>
</html>
