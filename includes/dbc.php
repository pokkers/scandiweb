<?php

class Dbc {

    private $servername;
    private $username;
    private $password;
    private $dbname;

//---Database connection method---//

    protected function connect() {

        $this->servername = "mysql";
        $this->username = "scandi";
        $this->password = "b8&4Tv3o";
        $this->dbname = "scandiwebs";

//---Create connection---//

        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);

//---Check for errors---//

        if($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

//---Get connection---//

        return $conn;
    }

}