<?php

class Products extends Dbc {

//---Getting all products from database---//

    public function getAllProducts() {

        $sql = "SELECT * FROM product";
        $result = $this->connect()->query($sql);
        $numRows = $result->num_rows;
        if($numRows > 0) {
            while($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        }
    }

//---Show all products---//

    public function showAllProducts() {
		
        $datas = $this->getAllProducts();
        foreach($datas as $data) {
            echo "<tr>";
            echo "<th>"."<input type='checkbox'name='num[]' value=".$data['id']."></th>";
            echo "<th>".$data['SKU']."</th>";
            echo "<th>".$data['Name']."</th>";
            echo "<th>".$data['Price']." &euro;"."</th>";
            echo "<th>".$data['Atribute'];
            if($data['Type']==1){
                echo "&nbsp;kg</th>"."</tr>";
            }
            elseif($data['Type']==2){
                echo "&nbsp;MB</th>"."</tr>";
            }
            else{
                echo "&nbsp;cm</th>"."</tr>";
            }
        }
    }

//---Insert products in database---//
	
	public function addProduct($sku, $name, $price, $type, $att) {
		
		$conn=$this->connect();
        $ins= "INSERT into product(SKU, Name, Price, Type ,Atribute) values('$sku', '$name', '$price', '$type', '$att')";
        if($run= mysqli_query($conn,$ins)){
            echo "<div class='alert alert-success' role='alert'> Product added!</div>";
        }
        else{
           echo "<div class='alert alert-danger' role='alert'>Product add failed: ".$conn->error."</div>";
        }
    }

//---Delete products with checkbox---//
    
    public function deleteProducts($val) {

        $conn=$this->connect();
        $del="DELETE from product where id=$val";
        $run=mysqli_query($conn,$del);  
    }

}