<?php
    include_once 'includes/dbc.php';
    include_once 'includes/products.php';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- main css -->
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>
    <!-- NAVIGATION -->
    <nav class="navbar navbar-expand navbar-dark bg-dark">
        <a class="navbar-brand" href="/">Products</a>
        <a class="navbar-brand" href="/add.php">Add products</a>
    </nav>
    <!-- end of NAVIGATION -->
    <!-- Product "show" and "delete" section-->
    <form name="delete" action="" method="POST">
        <div class="container">
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>SKU</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Atribute</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $products = new Products();
                            $products->showAllProducts();
                        ?>
                    </tbody>
                </table>
                <div class="col-12 mt-3">
                    <button class="col-3 btn btn-danger btn-lg btn-block" type="submit" name="delete-items">Bulk delete</button>
                </div>
            </div>
        </div>
    </form>
    <?php
    if(isset($_POST['delete-items'])) {
        $items=$_POST['num'];
        while (list ($key,$val) = @each ($items)){
            $products->deleteProducts($val);
        }
    ?>
    <script type="text/javascript">
        window.location.href=window.location.href;
    </script>
    <?php
    }
    ?>
    <!-- end of product "show" and "delete" section-->
    <!-- bootsrap jQuery js -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <!-- bootstrap js -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- bootsrap popper js -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
</body>
</html>
